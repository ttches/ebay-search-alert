const { APP_ID, MAILGUN_KEY, domain } = require('./keys');

const config = {
  // Unique config name v
  i5: {
    // Your shipping zip code
    shippingDestinationZipCode: '12345',
    // Email to receive alerts
    emailAddress: 'your-email@gmail.com',
    // Search parameters, [[Search phrase string, Max price number],[optional, condition, values]]
    // optional condition values: 'new', 'refurbished', 'used'. used includes refurbished
    params: [
      ['i5 2400', 100],
      ['i5 2500', 110],
    ]
    // Don't change anything below for this item ********************************************
      .map(mapParams),
    APP_ID,
    MAILGUN_KEY,
    domain,
  },
  rx560: {
    shippingDestinationZipCode: '12345',
    emailAddress: 'your-email@gmail.com',
    params: [
      ['rx 560 4gb', 80, ['used']],
      ['rx 560 4gb', 110, ['new', 'refurbished']],
    ]
    // Don't change anything below for this item ********************************************
      .map(mapParams),
    APP_ID,
    MAILGUN_KEY,
    domain,
  },
};

// DON'T TOUCH ANYTHING ELSE //////////////////////////

function reduceConditions(conditions) {
  const condition = {
    new: ['New'],
    used: ['Used'],
    refurbished: ['2000', '2500'],
  };
  return conditions.reduce((total, next) => {
    if (!condition[next]) throw "You've listed a condition that does not match 'new', 'used', or 'refurbished'.";
    return [...total, ...condition[next]];
  }, []);
}

function mapParams(params) {
  const mapped = {
    phrase: params[0],
    maxPrice: params[1],
  };
  if (params[2]) {
    mapped.conditions = reduceConditions(params[2]);
  }
  return mapped;
}

Object.keys(config).forEach((key) => {
  config[key].title = key;
});

module.exports = config;
