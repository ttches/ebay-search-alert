const fs = require('fs');

module.exports = {
  readFile: (file) => new Promise((res, rej) => {
    fs.readFile(file, 'utf8', (err, data) => {
      if (err) {
        module.exports.writeFile(file, JSON.stringify({}));
        res(err);
      } else {
        res(JSON.parse(data || "{}"));
      }
    })
  }),
  writeFile: (file, json) => fs.writeFile(file, json, 'utf8', () => null),
  composeData: (newItems, oldItems, file) => {
    const newObj = {};
    newItems.forEach(item => {
      newObj[item.itemId] = item;
    })
    module.exports.writeFile(file, JSON.stringify({ ...newObj, ...oldItems }));
  }
}
