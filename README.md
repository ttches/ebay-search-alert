This application will search ebay for your keywords, emailing items to you
if their buy price + shipping is set equal to or lower than your maxPrice.

##Setup:
create a file called keys.js, paste this code:

module.exports = {
  APP_ID: 'ebayAPI key',
  MAILGUN_KEY: 'mailgunAPI key',
  domain: 'mailgun domain',
};

You'll need to sign up for an API key with both ebay and mailgun and inject
your custom keys