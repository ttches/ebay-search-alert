const variables = require('./myVariables');

const configLength = Object.keys(variables).length;

const state = {
  configIndex: 0,
  running: false,
  configTitle: () => Object.keys(variables)[state.configIndex],
  getConfig: () => variables[state.configTitle()],
  nextConfig: () => {
    state.configIndex = (state.configIndex + 1) % configLength;
    console.log(`About to run config for ${state.configTitle()}`);
  },
};

module.exports = state;
