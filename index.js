const { getSearch, getShipping, apiCalls } = require('./requests');
const fs = require('./fs');
const { sendEmail } = require('./mailgun');
const Store = require('./store');


const { getConfig } = Store;

const getSearches = async () => {
  console.log('PARAMS', getConfig().params)
  // Resolve fetches
  const promises = await Promise.all(getConfig().params
    .map(({ phrase, maxPrice, conditions }) => getSearch(phrase, maxPrice, conditions)));
  const rawResponses = promises
    .filter(res => res.status === 200);
  let JSONresponses = await Promise.all(rawResponses.map(res => res.json()));
  JSONresponses = JSONresponses.filter(res => res.findItemsByKeywordsResponse[0].searchResult[0].item !== undefined); // eslint-disable-line
  if (!JSONresponses.length) {
    console.log('No items found');
    return;
  }
  // Add search value and for analytics
  JSONresponses.forEach((res, index) => {
    res.findItemsByKeywordsResponse[0].searchResult[0].item.forEach((item) => {
      item.searchValue = getConfig().params[index].phrase;
      item.maxPrice = getConfig().params[index].maxPrice;
    });
  });
  // Flatten array and remove unwanted data
  const responses = JSONresponses
    .reduce((total, next) => [...total, ...next.findItemsByKeywordsResponse[0].searchResult[0].item], []) // eslint-disable-line
    .map(res => ({
      itemId: res.itemId[0],
      title: res.title[0],
      galleryURL: res.galleryURL[0],
      viewItemURL: res.viewItemURL[0],
      shippingInfo: res.shippingInfo[0],
      sellingStatus: res.sellingStatus[0],
      returnsAccepted: res.returnsAccepted[0],
      searchValue: res.searchValue,
      maxPrice: res.maxPrice,
    }));
  const newItems = await filterSeenListings(responses);
  console.log(`${newItems.length} new items`);
  if (!newItems.length) {
    apiCalls.log();
    return;
  }
  const calculatedItems = await getTrueShippingPrice(newItems);
  const affordableItems = calculatedItems.filter(filterMaxPrice);
  console.log(`${affordableItems.length} affordable items`);
  if (!affordableItems.length) {
    apiCalls.log();
    return;
  }
  logAffordableItems(affordableItems);
  sendEmail(affordableItems);
  apiCalls.log();
};

const filterSeenListings = async (items) => {
  const oldItems = await fs.readFile('./dataIgnore.json');
  const newItems = items
    .filter(item => !oldItems[item.itemId]);
  fs.composeData(newItems, oldItems, './dataIgnore.json');
  return newItems;
};

const getTrueShippingPrice = async (items) => {
  console.log(`${items.length} total items`);
  const flat = [];
  const toCalc = [];
  items.forEach((item) => {
    const shippingType = item.shippingInfo.shippingType[0];
    if (shippingType === 'Free' || shippingType === 'Flat') flat.push(item);
    else toCalc.push(item);
  });
  console.log(`${toCalc.length} items to calculate shipping`);
  const calculated = await Promise.all(toCalc.map(calculateShipping));
  return [...flat, ...calculated].filter(item => item);
};

const calculateShipping = async (item) => {
  try {
    const { itemId } = item;
    const response = await getShipping(itemId);
    if (!response.ok) return null;
    const shippingDetails = await response.json();
    const shippingValue = shippingDetails.ShippingCostSummary.ShippingServiceCost.Value;
    if (!shippingValue) return null;
    item.shippingInfo = {
      shippingServiceCost: [{ __value__: shippingValue }],
    };
    return item;
  } catch (e) {
    return null;
  }
};

const filterMaxPrice = (item) => {
  const { maxPrice } = item;
  const price = Number(item.sellingStatus.convertedCurrentPrice[0].__value__);
  if (item.shippingInfo.shippingServiceCost) {
    return (
      (Number(item.shippingInfo.shippingServiceCost[0].__value__) + price) <= maxPrice
    );
  }
  return false;
};


const logAffordableItems = async (newItems) => {
  const oldItems = await fs.readFile('./data.json');
  fs.composeData(newItems, oldItems, './data.json');
};

((function init() {
  getSearches();
  setInterval(() => {
    try {
      if (new Date().toLocaleDateString() !== apiCalls.getDate()) apiCalls.resetDate();
      Store.nextConfig();
      getSearches();
    } catch (err) {
      console.log(err);
    }
  }, 60000);
})());
