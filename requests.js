const fetch = require('node-fetch');
const { getConfig } = require('./store');

const formatConditions = (conditions) => {
  if (!conditions) return '';
  const attr = `&itemFilter(2)`;
  conditions = conditions.reduce((total, next, i) =>
    `${total}${attr}.value(${i})=${next}`, '');
  return `${attr}.name=Condition${conditions}`;
};

const searchURL = (keyword, maxPrice, conditions) => {
  console.log('CONDITIONS', conditions);
  keyword = keyword.trim().replace(/ /g, '%20');
  const url = `http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=${getConfig().APP_ID}&RESPONSE-DATA-FORMAT=JSON&REST-PAYLOAD&keywords=${keyword}&sortOrder=StartTimeNewest&itemFilter(0).name=ListingType&itemFilter(0).value(0)=FixedPrice&itemFilter(0).value(1)=AuctionWithBIN&itemFilter(1).name=MaxPrice&itemFilter(1).value=${maxPrice}${formatConditions(conditions)}&paginationInput.entriesPerPage=100`;
  console.log(url);
  return url;
};

const shippingURL = (itemId) => {
  return `http://open.api.ebay.com/shopping?callname=GetShippingCosts&responseencoding=JSON&appid=${getConfig().APP_ID}&siteid=0&version=517&ItemID=${itemId}&DestinationCountryCode=US&DestinationPostalCode=${getConfig().shippingDestinationZipCode}&IncludeDetails=false&QuantitySold=1`
};

module.exports = {
  apiCalls: (() => {
    let count = 0;
    let date = new Date().toLocaleDateString();
    return {
      increase: () => {
        count += 1;
      },
      log: () => {
        console.log(`${count} API calls made today`);
      },
      getDate: () => date,
      resetCount: () => {
        count = 0;
      },
      resetDate: () => {
        date = new Date().toLocaleDateString();
      },
    };
  })(),
  getSearch: async (keyword, maxPrice, conditions) => {
    module.exports.apiCalls.increase();
    return fetch(searchURL(keyword, maxPrice, conditions));
  },
  getShipping: async (itemId) => {
    module.exports.apiCalls.increase();
    return fetch(shippingURL(itemId));
  },
};
