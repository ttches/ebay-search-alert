const Store = require('./store');
const { getConfig } = require('./store');

var mailgun = require('mailgun-js')({ apiKey: getConfig().MAILGUN_KEY, domain: getConfig().domain });

module.exports = {
  sendEmail: (items) => {
    console.log('email items', items);
    const uniqueItems = [];
    items.forEach(item => {
      if (uniqueItems.some(({ itemId }) => item.itemId === itemId)) return;
      uniqueItems.push(item);
    });
    console.log('Sending email');
    const formattedEmail = formatEmail(uniqueItems);
    mailgun.messages().send(formattedEmail, (error, body) => {
      console.log(body);
    });
  }
};

const formatEmail = (items) => {
  return {
    from: `Ebay Alert <Ty@ebayalerts.com>`,
    to: `${getConfig().emailAddress}`,
    subject: formatSubject(items),
    html: `<html>${formatHTML(items)}</html>`
  }
};

const formatSubject = (items) => {
  const searchTerms = items.map((item => item.searchValue));
  return [...new Set(searchTerms)].join(' ');
};

const formatHTML = (items) => {
  const innerHTML = items.map(item => {
    const shipping = Number(item.shippingInfo.shippingServiceCost[0].__value__);
    const price = Number(item.sellingStatus.convertedCurrentPrice[0].__value__);
    return `<div><a href='${item.viewItemURL}'>${item.title}</a></div>
    <div style='font-weight:bold'>${price + shipping}</div>
    <img src='${item.galleryURL}' />
    <div />`
  }).join(' ');
  return `<html>${innerHTML}</html>`
};
